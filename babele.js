Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'l5r5e-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });        
    }
});

